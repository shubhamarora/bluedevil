msgid ""
msgstr ""
"Project-Id-Version: bluedevil\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-06-02 02:04+0000\n"
"PO-Revision-Date: 2022-07-26 09:25+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-POFile-SpellExtra: Fella NAP DUN\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "José Nuno Pires"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "zepires@gmail.com"

#: bluetooth.cpp:101
#, kde-format
msgctxt "DeviceName Network (Service)"
msgid "%1 Network (%2)"
msgstr "Rede %1 (%2)"

#: package/contents/ui/Device.qml:68 package/contents/ui/main.qml:231
#, kde-format
msgid "Disconnect"
msgstr "Desligar"

#: package/contents/ui/Device.qml:68 package/contents/ui/main.qml:231
#, kde-format
msgid "Connect"
msgstr "Ligar"

#: package/contents/ui/Device.qml:100
#, kde-format
msgid "Type:"
msgstr "Tipo:"

#: package/contents/ui/Device.qml:105 package/contents/ui/General.qml:42
#, kde-format
msgid "Address:"
msgstr "Endereço:"

#: package/contents/ui/Device.qml:110
#, kde-format
msgid "Adapter:"
msgstr "Adaptador:"

#: package/contents/ui/Device.qml:116 package/contents/ui/General.qml:36
#, kde-format
msgid "Name:"
msgstr "Nome:"

#: package/contents/ui/Device.qml:120
#, kde-format
msgid "Trusted"
msgstr "Fidedigno"

#: package/contents/ui/Device.qml:126
#, kde-format
msgid "Blocked"
msgstr "Bloqueado"

#: package/contents/ui/Device.qml:132
#, kde-format
msgid "Send File"
msgstr "Enviar o Ficheiro"

#: package/contents/ui/Device.qml:139
#, kde-format
msgid "Setup NAP Network…"
msgstr "Configurar uma Rede NAP…"

#: package/contents/ui/Device.qml:146
#, kde-format
msgid "Setup DUN Network…"
msgstr "Configurar uma Rede DUN…"

#: package/contents/ui/Device.qml:156
#, kde-format
msgctxt "This device is a Phone"
msgid "Phone"
msgstr "Telefone"

#: package/contents/ui/Device.qml:158
#, kde-format
msgctxt "This device is a Modem"
msgid "Modem"
msgstr "Modem"

#: package/contents/ui/Device.qml:160
#, kde-format
msgctxt "This device is a Computer"
msgid "Computer"
msgstr "Computador"

#: package/contents/ui/Device.qml:162
#, kde-format
msgctxt "This device is of type Network"
msgid "Network"
msgstr "Rede"

#: package/contents/ui/Device.qml:164
#, kde-format
msgctxt "This device is a Headset"
msgid "Headset"
msgstr "Auriculares"

#: package/contents/ui/Device.qml:166
#, kde-format
msgctxt "This device is a Headphones"
msgid "Headphones"
msgstr "Auscultadores"

#: package/contents/ui/Device.qml:168
#, kde-format
msgctxt "This device is an Audio/Video device"
msgid "Multimedia Device"
msgstr "Dispositivo Multimédia"

#: package/contents/ui/Device.qml:170
#, kde-format
msgctxt "This device is a Keyboard"
msgid "Keyboard"
msgstr "Teclado"

#: package/contents/ui/Device.qml:172
#, kde-format
msgctxt "This device is a Mouse"
msgid "Mouse"
msgstr "Rato"

#: package/contents/ui/Device.qml:174
#, kde-format
msgctxt "This device is a Joypad"
msgid "Joypad"
msgstr "Comando de jogos"

#: package/contents/ui/Device.qml:176
#, kde-format
msgctxt "This device is a Graphics Tablet (input device)"
msgid "Tablet"
msgstr "Tablete"

#: package/contents/ui/Device.qml:178
#, kde-format
msgctxt "This device is a Peripheral device"
msgid "Peripheral"
msgstr "Periférico"

#: package/contents/ui/Device.qml:180
#, kde-format
msgctxt "This device is a Camera"
msgid "Camera"
msgstr "Câmara"

#: package/contents/ui/Device.qml:182
#, kde-format
msgctxt "This device is a Printer"
msgid "Printer"
msgstr "Impressora"

#: package/contents/ui/Device.qml:184
#, kde-format
msgctxt ""
"This device is an Imaging device (printer, scanner, camera, display, …)"
msgid "Imaging"
msgstr "Imagens"

#: package/contents/ui/Device.qml:186
#, kde-format
msgctxt "This device is a Wearable"
msgid "Wearable"
msgstr "Acessório"

#: package/contents/ui/Device.qml:188
#, kde-format
msgctxt "This device is a Toy"
msgid "Toy"
msgstr "Brinquedo"

#: package/contents/ui/Device.qml:190
#, kde-format
msgctxt "This device is a Health device"
msgid "Health"
msgstr "Saúde"

#: package/contents/ui/Device.qml:192
#, kde-format
msgctxt "Type of device: could not be determined"
msgid "Unknown"
msgstr "Desconhecido"

#: package/contents/ui/General.qml:19
#, kde-format
msgid "Settings"
msgstr "Configuração"

#: package/contents/ui/General.qml:28
#, kde-format
msgid "Device:"
msgstr "Dispositivo:"

#: package/contents/ui/General.qml:46
#, kde-format
msgid "Enabled:"
msgstr "Activo:"

#: package/contents/ui/General.qml:52
#, kde-format
msgid "Visible:"
msgstr "Visível:"

#: package/contents/ui/General.qml:66
#, kde-format
msgid "On login:"
msgstr "No início da sessão:"

#: package/contents/ui/General.qml:67
#, kde-format
msgid "Enable Bluetooth"
msgstr "Activar o Bluetooth"

#: package/contents/ui/General.qml:77
#, kde-format
msgid "Disable Bluetooth"
msgstr "Desactivar o Bluetooth"

#: package/contents/ui/General.qml:87
#, kde-format
msgid "Restore previous status"
msgstr "Repor o estado anterior"

#: package/contents/ui/General.qml:106
#, kde-format
msgid "When receiving files:"
msgstr "Ao receber ficheiros:"

#: package/contents/ui/General.qml:108
#, kde-format
msgid "Ask for confirmation"
msgstr "Pedir a confirmação"

#: package/contents/ui/General.qml:117
#, kde-format
msgid "Accept for trusted devices"
msgstr "Aceitar para os dispositivos fidedignos"

#: package/contents/ui/General.qml:127
#, kde-format
msgid "Always accept"
msgstr "Aceitar sempre"

#: package/contents/ui/General.qml:137
#, kde-format
msgid "Save files in:"
msgstr "Gravar os ficheiros em:"

#: package/contents/ui/General.qml:161 package/contents/ui/General.qml:172
#, kde-format
msgid "Select folder"
msgstr "Seleccionar a pasta"

#: package/contents/ui/main.qml:24
#, fuzzy, kde-format
#| msgid "Enabled:"
msgctxt "@action: button as in, 'enable Bluetooth'"
msgid "Enabled"
msgstr "Activo:"

#: package/contents/ui/main.qml:38
#, kde-format
msgid "Add New Device…"
msgstr "Adicionar um Novo Dispositivo…"

#: package/contents/ui/main.qml:44
#, kde-format
msgid "Configure…"
msgstr "Configurar…"

#: package/contents/ui/main.qml:83
#, kde-format
msgid "Forget this Device?"
msgstr "Esquecer este Dispositivo?"

#: package/contents/ui/main.qml:84
#, kde-format
msgid "Are you sure you want to forget \"%1\"?"
msgstr "Tem a certeza que deseja esquecer o \"%1\"?"

#: package/contents/ui/main.qml:95
#, kde-format
msgctxt "@action:button"
msgid "Forget Device"
msgstr "Esquecer o Dispositivo"

#: package/contents/ui/main.qml:102
#, kde-format
msgctxt "@action:button"
msgid "Cancel"
msgstr "Cancelar"

#: package/contents/ui/main.qml:163
#, kde-format
msgid "No Bluetooth adapters found"
msgstr "Nenhum adaptador Bluetooth detectado"

#: package/contents/ui/main.qml:172
#, kde-format
msgid "Bluetooth is disabled"
msgstr "O Bluetooth está desactivado"

#: package/contents/ui/main.qml:178
#, kde-format
msgid "Enable"
msgstr "Activar"

#: package/contents/ui/main.qml:188
#, kde-format
msgid "No devices paired"
msgstr "Sem dispositivos emparelhados"

#: package/contents/ui/main.qml:209
#, kde-format
msgid "Connected"
msgstr "Ligado"

#: package/contents/ui/main.qml:209
#, kde-format
msgid "Available"
msgstr "Disponível"

#: package/contents/ui/main.qml:242
#, kde-format
msgctxt "@action:button %1 is the name of a Bluetooth device"
msgid "Forget \"%1\""
msgstr "Esquecer o \"%1\""
