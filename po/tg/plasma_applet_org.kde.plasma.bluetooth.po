# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the bluedevil package.
#
# Victor Ibragimov <victor.ibragimov@gmail.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: bluedevil\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-04-26 02:16+0000\n"
"PO-Revision-Date: 2019-10-01 10:11+0500\n"
"Last-Translator: Victor Ibragimov <victor.ibragimov@gmail.com>\n"
"Language-Team: English <kde-i18n-doc@kde.org>\n"
"Language: tg\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.3\n"

#: package/contents/ui/DeviceItem.qml:34
#, kde-format
msgid "Disconnect"
msgstr ""

#: package/contents/ui/DeviceItem.qml:34
#, kde-format
msgid "Connect"
msgstr ""

#: package/contents/ui/DeviceItem.qml:43
#, kde-format
msgid "Browse Files"
msgstr ""

#: package/contents/ui/DeviceItem.qml:54
#, kde-format
msgid "Send File"
msgstr ""

#: package/contents/ui/DeviceItem.qml:114
#, kde-format
msgid "Copy"
msgstr ""

#: package/contents/ui/DeviceItem.qml:210
#, kde-format
msgid "Yes"
msgstr "Ҳа"

#: package/contents/ui/DeviceItem.qml:210
#, kde-format
msgid "No"
msgstr "Не"

#: package/contents/ui/DeviceItem.qml:216
#, kde-format
msgctxt "@label %1 is human-readable adapter name, %2 is HCI"
msgid "%1 (%2)"
msgstr ""

#: package/contents/ui/DeviceItem.qml:224
#, kde-format
msgid "Remote Name"
msgstr "Номи дурдаст"

#: package/contents/ui/DeviceItem.qml:228
#, kde-format
msgid "Address"
msgstr "Нишонӣ"

#: package/contents/ui/DeviceItem.qml:231
#, kde-format
msgid "Paired"
msgstr ""

#: package/contents/ui/DeviceItem.qml:234
#, kde-format
msgid "Trusted"
msgstr ""

#: package/contents/ui/DeviceItem.qml:237
#, kde-format
msgid "Adapter"
msgstr "Адаптер"

#: package/contents/ui/DeviceItem.qml:245
#, kde-format
msgid "Disconnecting"
msgstr ""

#: package/contents/ui/DeviceItem.qml:245
#, kde-format
msgid "Connecting"
msgstr ""

#: package/contents/ui/DeviceItem.qml:251
#, kde-format
msgid "Connected"
msgstr ""

#: package/contents/ui/DeviceItem.qml:258
#, kde-format
msgid "Audio device"
msgstr ""

#: package/contents/ui/DeviceItem.qml:265
#, kde-format
msgid "Input device"
msgstr ""

#: package/contents/ui/DeviceItem.qml:269
#, kde-format
msgid "Phone"
msgstr ""

#: package/contents/ui/DeviceItem.qml:276
#, kde-format
msgid "File transfer"
msgstr ""

#: package/contents/ui/DeviceItem.qml:279
#, kde-format
msgid "Send file"
msgstr ""

#: package/contents/ui/DeviceItem.qml:282
#, kde-format
msgid "Input"
msgstr ""

#: package/contents/ui/DeviceItem.qml:285
#, kde-format
msgid "Audio"
msgstr "Аудио"

#: package/contents/ui/DeviceItem.qml:288
#, kde-format
msgid "Network"
msgstr "Шабака"

#: package/contents/ui/DeviceItem.qml:292
#, kde-format
msgid "Other device"
msgstr ""

#: package/contents/ui/DeviceItem.qml:299 package/contents/ui/main.qml:64
#: package/contents/ui/main.qml:74
#, kde-format
msgid "%1% Battery"
msgstr ""

#: package/contents/ui/DeviceItem.qml:310
#, kde-format
msgctxt "Notification when the connection failed due to Failed:HostIsDown"
msgid "The device is unreachable"
msgstr ""

#: package/contents/ui/DeviceItem.qml:312
#, kde-format
msgctxt "Notification when the connection failed due to Failed"
msgid "Connection to the device failed"
msgstr ""

#: package/contents/ui/DeviceItem.qml:316
#, kde-format
msgctxt "Notification when the connection failed due to NotReady"
msgid "The device is not ready"
msgstr ""

#: package/contents/ui/DeviceItem.qml:350
#, kde-format
msgctxt ""
"@label %1 is human-readable device name, %2 is low-level device address"
msgid "%1 (%2)"
msgstr ""

#: package/contents/ui/FullRepresentation.qml:58
#, kde-format
msgid "Enable"
msgstr ""

#: package/contents/ui/FullRepresentation.qml:148
#, kde-format
msgid "No Bluetooth adapters available"
msgstr ""

#: package/contents/ui/FullRepresentation.qml:150
#, kde-format
msgid "Bluetooth is disabled"
msgstr ""

#: package/contents/ui/FullRepresentation.qml:152
#, kde-format
msgid "No devices found"
msgstr ""

#: package/contents/ui/main.qml:42
#, kde-format
msgid "Bluetooth"
msgstr "Bluetooth"

#: package/contents/ui/main.qml:45
#, kde-format
msgid "Bluetooth is disabled; middle-click to enable"
msgstr ""

#: package/contents/ui/main.qml:49
#, kde-format
msgid "No adapters available"
msgstr ""

#: package/contents/ui/main.qml:51
#, kde-format
msgid "Bluetooth is offline"
msgstr ""

#: package/contents/ui/main.qml:54
#, kde-format
msgid "Middle-click to disable Bluetooth"
msgstr ""

#: package/contents/ui/main.qml:57
#, kde-format
msgid "No connected devices"
msgstr ""

#: package/contents/ui/main.qml:62
#, kde-format
msgid "%1 connected"
msgstr ""

#: package/contents/ui/main.qml:69
#, kde-format
msgctxt "Number of connected devices"
msgid "%1 connected device"
msgid_plural "%1 connected devices"
msgstr[0] ""
msgstr[1] ""

#: package/contents/ui/main.qml:141
#, fuzzy, kde-format
#| msgid "Configure Bluetooth..."
msgid "Configure &Bluetooth…"
msgstr "Танзимоти Bluetooth..."

#: package/contents/ui/main.qml:143
#, kde-format
msgid "Add New Device…"
msgstr ""

#: package/contents/ui/main.qml:146 package/contents/ui/Toolbar.qml:32
#, kde-format
msgid "Enable Bluetooth"
msgstr ""
