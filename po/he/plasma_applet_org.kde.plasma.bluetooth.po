# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Elkana Bardugo <ttv200@gmail.com>, 2016.
# Elkana Bardugo <ttv200@gmail.com>, 2017. #zanata
msgid ""
msgstr ""
"Project-Id-Version: plasma_applet_org.kde.plasma.bluetooth\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-04-26 02:16+0000\n"
"PO-Revision-Date: 2017-05-16 06:55-0400\n"
"Last-Translator: Copied by Zanata <copied-by-zanata@zanata.org>\n"
"Language-Team: Hebrew <kde-i18n-doc@kde.org>\n"
"Language: he\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Zanata 3.9.6\n"

#: package/contents/ui/DeviceItem.qml:34
#, kde-format
msgid "Disconnect"
msgstr "התנתק"

#: package/contents/ui/DeviceItem.qml:34
#, kde-format
msgid "Connect"
msgstr "התחבר"

#: package/contents/ui/DeviceItem.qml:43
#, kde-format
msgid "Browse Files"
msgstr "דפדפף בקבצים"

#: package/contents/ui/DeviceItem.qml:54
#, kde-format
msgid "Send File"
msgstr "שלח קובץ"

#: package/contents/ui/DeviceItem.qml:114
#, kde-format
msgid "Copy"
msgstr ""

#: package/contents/ui/DeviceItem.qml:210
#, kde-format
msgid "Yes"
msgstr "כן"

#: package/contents/ui/DeviceItem.qml:210
#, kde-format
msgid "No"
msgstr "לא"

#: package/contents/ui/DeviceItem.qml:216
#, kde-format
msgctxt "@label %1 is human-readable adapter name, %2 is HCI"
msgid "%1 (%2)"
msgstr ""

#: package/contents/ui/DeviceItem.qml:224
#, kde-format
msgid "Remote Name"
msgstr "שם מרוחק"

#: package/contents/ui/DeviceItem.qml:228
#, kde-format
msgid "Address"
msgstr "כתובת"

#: package/contents/ui/DeviceItem.qml:231
#, kde-format
msgid "Paired"
msgstr "מתואם"

#: package/contents/ui/DeviceItem.qml:234
#, kde-format
msgid "Trusted"
msgstr "אמין"

#: package/contents/ui/DeviceItem.qml:237
#, kde-format
msgid "Adapter"
msgstr "מתאם"

#: package/contents/ui/DeviceItem.qml:245
#, kde-format
msgid "Disconnecting"
msgstr "מתנתק"

#: package/contents/ui/DeviceItem.qml:245
#, kde-format
msgid "Connecting"
msgstr "מתחבר"

#: package/contents/ui/DeviceItem.qml:251
#, fuzzy, kde-format
#| msgid "Connect"
msgid "Connected"
msgstr "התחבר"

#: package/contents/ui/DeviceItem.qml:258
#, kde-format
msgid "Audio device"
msgstr "התקן שמע"

#: package/contents/ui/DeviceItem.qml:265
#, kde-format
msgid "Input device"
msgstr "התקן קלט"

#: package/contents/ui/DeviceItem.qml:269
#, kde-format
msgid "Phone"
msgstr ""

#: package/contents/ui/DeviceItem.qml:276
#, kde-format
msgid "File transfer"
msgstr "העברת קבצים"

#: package/contents/ui/DeviceItem.qml:279
#, kde-format
msgid "Send file"
msgstr "שלח קובץ"

#: package/contents/ui/DeviceItem.qml:282
#, kde-format
msgid "Input"
msgstr "קלט"

#: package/contents/ui/DeviceItem.qml:285
#, kde-format
msgid "Audio"
msgstr "שמע"

#: package/contents/ui/DeviceItem.qml:288
#, kde-format
msgid "Network"
msgstr "רשת"

#: package/contents/ui/DeviceItem.qml:292
#, kde-format
msgid "Other device"
msgstr "התקנים אחרים"

#: package/contents/ui/DeviceItem.qml:299 package/contents/ui/main.qml:64
#: package/contents/ui/main.qml:74
#, kde-format
msgid "%1% Battery"
msgstr ""

#: package/contents/ui/DeviceItem.qml:310
#, kde-format
msgctxt "Notification when the connection failed due to Failed:HostIsDown"
msgid "The device is unreachable"
msgstr "ההתקן אינו ניתן להשגה"

#: package/contents/ui/DeviceItem.qml:312
#, kde-format
msgctxt "Notification when the connection failed due to Failed"
msgid "Connection to the device failed"
msgstr "ההתחברות להתקן נכשלה"

#: package/contents/ui/DeviceItem.qml:316
#, kde-format
msgctxt "Notification when the connection failed due to NotReady"
msgid "The device is not ready"
msgstr "ההתקן אינו מוכן"

#: package/contents/ui/DeviceItem.qml:350
#, kde-format
msgctxt ""
"@label %1 is human-readable device name, %2 is low-level device address"
msgid "%1 (%2)"
msgstr ""

#: package/contents/ui/FullRepresentation.qml:58
#, kde-format
msgid "Enable"
msgstr ""

#: package/contents/ui/FullRepresentation.qml:148
#, fuzzy, kde-format
#| msgid "No Adapters Available"
msgid "No Bluetooth adapters available"
msgstr "אין מתאמים זמינים"

#: package/contents/ui/FullRepresentation.qml:150
#, kde-format
msgid "Bluetooth is disabled"
msgstr "הבלוטוס כבוי"

#: package/contents/ui/FullRepresentation.qml:152
#, fuzzy, kde-format
#| msgid "No Devices Found"
msgid "No devices found"
msgstr "לא נמצאו התקנים"

#: package/contents/ui/main.qml:42
#, kde-format
msgid "Bluetooth"
msgstr "בלוטוס"

#: package/contents/ui/main.qml:45
#, fuzzy, kde-format
#| msgid "Bluetooth is disabled"
msgid "Bluetooth is disabled; middle-click to enable"
msgstr "הבלוטוס כבוי"

#: package/contents/ui/main.qml:49
#, kde-format
msgid "No adapters available"
msgstr "אין מתאמים זמינים"

#: package/contents/ui/main.qml:51
#, kde-format
msgid "Bluetooth is offline"
msgstr "הבלוטוס לא מחובר"

#: package/contents/ui/main.qml:54
#, kde-format
msgid "Middle-click to disable Bluetooth"
msgstr ""

#: package/contents/ui/main.qml:57
#, kde-format
msgid "No connected devices"
msgstr "אין התקנים מחוברים"

#: package/contents/ui/main.qml:62
#, fuzzy, kde-format
#| msgctxt "Number of connected devices"
#| msgid "%1 connected device"
#| msgid_plural "%1 connected devices"
msgid "%1 connected"
msgstr "התקן מחובר"

#: package/contents/ui/main.qml:69
#, kde-format
msgctxt "Number of connected devices"
msgid "%1 connected device"
msgid_plural "%1 connected devices"
msgstr[0] "התקן מחובר"
msgstr[1] "%1 התקנים מחוברים"

#: package/contents/ui/main.qml:141
#, fuzzy, kde-format
#| msgid "Configure &Bluetooth..."
msgid "Configure &Bluetooth…"
msgstr "הגדר בלוטוס..."

#: package/contents/ui/main.qml:143
#, fuzzy, kde-format
#| msgid "Add New Device"
msgid "Add New Device…"
msgstr "הוסף התקן חדש"

#: package/contents/ui/main.qml:146 package/contents/ui/Toolbar.qml:32
#, kde-format
msgid "Enable Bluetooth"
msgstr "הפעל בלוטוס"

#~ msgid "Bluetooth is Disabled"
#~ msgstr "הבלוטוס כבוי"

#~ msgid "Add New Device..."
#~ msgstr "הוסף התקן חדש..."

#~ msgid "Connected devices"
#~ msgstr "התקנים מחוברים"

#~ msgid "Available devices"
#~ msgstr "התקנים זמינים"

#~ msgid "Configure Bluetooth..."
#~ msgstr "הגדרות בלוטוס..."
